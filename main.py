import pyautogui
import time

pyautogui.useImageNotFoundException()

menu_location = pyautogui.locateCenterOnScreen(
    "images/menu.png",
    confidence=0.9,
    grayscale=True,
)

pyautogui.click(
    menu_location.x, menu_location.y
)

time.sleep(1)

system_monitor = pyautogui.locateCenterOnScreen(
    "images/system_monitor.png",
    confidence=0.9,
    grayscale=True,
)

pyautogui.click(
    system_monitor.x, system_monitor.y
)

time.sleep(1)

pyautogui.screenshot(
    '/home/rafael/Desktop/system_monitor_screenshot.png',
)

slack_tray_location = pyautogui.locateCenterOnScreen(
    "images/slack_tray.png",
    confidence=0.9,
    grayscale=True,
)

pyautogui.click(
    slack_tray_location.x, slack_tray_location.y
)

time.sleep(1)

slack_me_location = pyautogui.locateCenterOnScreen(
    "images/slack_me.png",
    confidence=0.9,
    grayscale=True,
)

pyautogui.click(
    slack_me_location.x, slack_me_location.y
)

time.sleep(1)

slack_attach_location = pyautogui.locateCenterOnScreen(
    "images/slack_attach.png",
    confidence=0.9,
    grayscale=True,
)

pyautogui.doubleClick(
    slack_attach_location.x, slack_attach_location.y
)

time.sleep(1)

file_picker_desktop_location = pyautogui.locateCenterOnScreen(
    "images/file_picker_desktop.png",
    confidence=0.9,
    grayscale=True,
)

pyautogui.click(
    file_picker_desktop_location.x, file_picker_desktop_location.y
)

time.sleep(1)

file_picker_file_location = pyautogui.locateCenterOnScreen(
    "images/file_picker_file.png",
    confidence=0.9,
    grayscale=True,
)

pyautogui.doubleClick(
    file_picker_file_location.x, file_picker_file_location.y
)

time.sleep(1)

pyautogui.write('Aqui está relatório do dia! :pepeyeah:')

time.sleep(0.5)

pyautogui.press('enter')
